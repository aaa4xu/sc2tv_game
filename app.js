//require('./game/');
var Game = require('./game/');
var Chat = require('./chat/');

var game = new Game();
var chat = new Chat();

var command = process.argv[2];

if(command == 'chat'){
    chat.connect();
}
if(command == 'game'){
    game.start();
}

if(command == 'run'){
    game.start();
    chat.connect();
}