var io = require('socket.io-client');
var sockjs = require('sockjs-client');
var console = require('mag')('Chat.GoodGame');
var _ = require('underscore');
var EventEmitter = require('events').EventEmitter;
var util = require('util');

module.exports = function GoodGameListener(cfg, service){
    var self = this;

    var sock;
    var messages = null;
    var last_message_timestamp = null;

    // send request to API server
    self.send = function (request) {
        sock.send(JSON.stringify(request));
    }

    self.throw_error = function (e) {
        console.log(e.errorMsg);
    }

    // route responses
    self.route = function (e){
        var response = JSON.parse(e.data);
        console.log('Response type: ' + response.type);
        if(typeof self[response.type] == 'function'){
            self[response.type](response.data);
        }else{
            console.log('Response from server hasn\'t handler: ' + response.type);
        }
    }

    self.error = function (data) {
        self.throw_error(data);
    }

    self.welcome = function (data){
        console.log('API version: ' + data.protocolVersion);
    }

    // new message broadcasted ti all channel's clients
    self.message = function (data) {
        // If message is not command - ignore it
        if(data.text.substr(0, cfg.prefix.length) !== cfg.prefix) return;

        // Fire command event
        self.emit('command', {
            service: service,
            text: data.text.substr(cfg.prefix.length),
            userId: data.user_id
        });
    }

    // channel's users list
    self.users_list = function (data) {
        var users = [];
        _.each(data.users, function(element, key){
            users.push(element.name);
        });
        console.log('Users in channel (' + users.length + '): ' + users.join(', '));
    }

    // channel's users count
    self.channel_counters = function (data) {
        console.log('Clients in channel: ' + data.clients_in_channel);
        console.log('Users in channel: ' + data.users_in_channel);
    }

    // parse new messages from channel chat history
    self.parse_messages_in_range = function (from){
        var new_messages = [];

        _.each(messages, function(message, key){
            if(message.timestamp > from){
                new_messages.push(message);
            }
        });

        console.log('New messages (' + new_messages.length + '): ' + JSON.stringify(new_messages));

        _.each(new_messages, function (message, key) {
            add_vote(commands.vote, message.text, message.user_id, service);
        })
    }

    // channel chat history
    self.channel_history = function (data) {
        messages = data.messages;
        var response_timestamp = messages.reverse()[0]['timestamp'];
        if(response_timestamp > last_message_timestamp){
            self.parse_messages_in_range(last_message_timestamp);
            last_message_timestamp = response_timestamp;
        }
        //console.log('Channel history: ' + JSON.stringify(messages));
        //console.log('Last message time: ' + new Date(last_message_timestamp*1000));
    }

    // requests to API

    // join to channel
    self.join_channel = function(){
        var request = {
            type: "join",
            data: {
                channel_id: cfg.channel_id,
                hidden: false
            }
        }
        self.send(request);
    }

    // get channel history
    self.get_channel_history = function () {
        var request = {
            type: "get_channel_history",
            data: {
                channel_id: cfg.channel_id
            }
        }
        self.send(request);
    }
    
    // get current channel clients and users count
    self.get_channel_counters = function(){
        var request = {
            type: "get_channel_counters",
            data: {
                channel_id: cfg.channel_id
            }
        }
        self.send(request);
    }

    // get current channel users count
    self.get_users_list = function () {
        var request = {
            type: "get_users_list2",
            data: {
                channel_id: cfg.channel_id
            }
        };
        self.send(request);
    }

    // listen chat history every 5 seconds
    self.listen_chat = function(){
        setInterval(self.get_channel_history, 5000);
    }

    // start channel listening
    self.listen = function () {
        sock = new sockjs(cfg.api_url);
        sock.onopen = function() {
            console.log("Connected ");
            self.join_channel();
        };
        sock.onmessage = function(e) {
            self.route(e);
        };
        sock.onclose = function() {
            console.log('Connection closed');
        };
    }

    return self;
}

// GoodGameListener extends EventEmitter
util.inherits(module.exports, EventEmitter);