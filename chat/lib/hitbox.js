var io = require('socket.io-client');
var request = require('request');
var hitbox = require('hitbox-chat');
var console = require('mag')('Hitbox');
var EventEmitter = require('events').EventEmitter;
var util = require('util');

module.exports = hitboxListener;


function hitboxListener(cfg, service){
    var self = this;
    var auth_token;
    var chat_server = null;

    self.get_auth_token = function () {
        var post_data = {
            url: cfg.api_url + '/auth/token',
            body: {
                login: cfg.username,
                pass: cfg.password,
                app: cfg.app
            }
        }

        request.post(post_data, function(error, response, body){
            auth_token = body.authToken;
        });
    }

    self.connect = function () {
        var client = new hitbox({user: cfg.username, pass: cfg.password});
        client.on('connect', function () {
            var channel = client.joinChannel(cfg.channel_id);
            channel.on("login", function(name, role) {
                console.log('Name: ' + name);
                console.log('Role: ' + role);
            }).on("chat", function(name, text, role) {
                // If message is not command - ignore it
                if(text.substr(0, cfg.prefix.length) !== cfg.prefix) return;

                // Fire command event
                self.emit('command', {
                    service: service,
                    text: text.substr(cfg.prefix.length),
                    userId: name
                });
            }).on("other", function(method, params) {
                console.log('Some unhandled event ' + method);
            });
        }).on("disconnect", function() {
            console.log('Disconnected');
        });
    }

    self.get_chat_server = function(){
        request(cfg.api_url + 'chat/servers?redis=true', function (error, response, body) {
            var servers = JSON.parse(body);
            var i = -1;
            (function next() {
                i = (i + 1) % servers.length;
                var sock = io.connect("http://" + servers[i].server_ip, { timeout: 5000 });
                sock.on("connect", function (data) {
                    //sock.emit('connect');
                    console.log("Connect: ", data);
                });
                sock.on("connect_timeout", next);
                sock.on("error", next);
            })();
        });
    }

    self.listen = function(){
        //self.get_chat_server();
        self.connect();

        /*if(chat_server == null){
            console.log('Can\'t select chat server');
            return;
        }*/



    }

    return self;
}

// GoodGameListener extends EventEmitter
util.inherits(module.exports, EventEmitter);