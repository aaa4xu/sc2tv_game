var irc = require('irc');
var console = require('mag')('Chat.Twitch');
var EventEmitter = require('events').EventEmitter;
var util = require('util');

module.exports = function TwitchListener(cfg, service){
    var self = this;

    var client;
    var irc_opt = {
        channels: ['#' + cfg.channel_id]
    }

    if(cfg.token) {
        irc_opt.password = cfg.token ? 'oauth:' + cfg.token : 'blah';
    }

    self.listen = function () {
        client = new irc.Client(cfg.api_url, cfg.username || ('justinfan'+ Math.round(Math.random() * 100000)), irc_opt);

        client.addListener('message', function(from, to, message){
            // If message is not command - ignore it
            if(message.substr(0, cfg.prefix.length) !== cfg.prefix) return;

            // Fire command event
            self.emit('command', {
                service: service,
                text: message.substr(cfg.prefix.length),
                userId: from
            });
        });

        client.addListener('error', function(message) {
            console.log('Error: ', message);
        });
    }

    return self;
}

// TwitchListener extends EventEmitter
util.inherits(module.exports, EventEmitter);