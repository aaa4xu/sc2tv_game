var io = require('socket.io-client');
var console = require('mag')('Chat.Funstream');
var EventEmitter = require('events').EventEmitter;
var util = require('util');

module.exports = function FunstreamListener(cfg, service){
    var self = this;

    // start channel listening
    self.listen = function () {
        var socket = io.connect(cfg.api_url, {transports: ['websocket']});

        socket.on('connect', function() {
            console.log('Connected to server');

            socket.emit('/chat/join', {channel: "stream/" + cfg.channel_id}, function () {
                console.log('Connected to channel #'+ cfg.channel_id);
            });

            socket.on('/chat/message', function (data) {
                // If message is not command - ignore it
                if(data.text.substr(0, cfg.prefix.length) !== cfg.prefix) return;

                // Fire command event
                self.emit('command', {
                    service: service,
                    text: data.text.substr(cfg.prefix.length),
                    userId: data.from.id
                });
            });
        });
    }

    return self;
}

// FunstreamListener extends EventEmitter
util.inherits(module.exports, EventEmitter);