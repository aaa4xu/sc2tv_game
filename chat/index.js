'use strict';
var _ = require('underscore');
var cfg = require('./config');
var console = require('mag')('Chat');
var io = require('socket.io-client');
module.exports = ChatClient;

function ChatClient () {
    var self = this;
    var game_socket;
    var users = {};
    var users_count = null;
    var votes = [];
    var active = true;
    var poll = []; // votes goal for each poll option

    self.connect_services = function () {
        _.each(cfg, function(value, key){
            var Chat = require('./lib/' + key);
            var chat = new Chat(value, key);

            chat.on('command', self.process_command.bind(self));
            chat.listen();
        });
    }

    self.process_command = function(data) {
        // Very simple command handler
        var command = data.text.split(' ');

        if(command[0] === 'vote') {
            self.add_vote(data.service, parseInt(command[1], 10), data.userId);
            return;
        }

        console.debug('Unknown command: '+ command[0]);
    }

    // fill available votes option from Poll object taken from game server
    self.fill_votes = function () {
        for(var i = 0; i < poll.length; i++){
            votes[i] = 0;
        }
    }

    self.check_winner = function (index) {
        if(votes[index] >= poll[index]){
            active = false;
            game_socket.emit('poll_end', {index: index});
        }
    }

    // reset poll state on new poll start event given from game server
    self.reset = function (data) {
        votes = [];
        Object.keys(data.poll).map(function (value, index) {
            poll[index] = data.poll[index]['weight'];
        });
        _.each(cfg, function(value, key){
            users[key] = [];
        });
        self.fill_votes();
        console.log('Data from server: ' + JSON.stringify(data) + ' \nPoll: ' + poll);
        active = true;
    }

    // calc commans
    self.add_vote = function (service, vote, user){
        if(!active){
            return;
        }
        if(users[service].indexOf(user) != -1){
            return;
        }

        if(vote > 0 && vote <= poll.length){
            var index = vote - 1;
            //console.log('vote counted');
            //console.log(self.votes);
            votes[index]++; // increment vote counter for poll option;
            users[service].push(user);

            console.log('Votes: ' + votes);
            self.check_winner(index);
        }
    };

    // start socket.io client
    self.start_io = function () {
        game_socket = io.connect('http://localhost:3002', {transports: ['websocket']});
        game_socket.on('connect', function (data) {
            game_socket.emit('join');
        })
        game_socket.on('poll_start', function (data) {
            self.reset(data);
            console.log('Client: New poll started');
        })
    };

    self.connect = function () {
        self.connect_services();
        self.start_io();
        console.log('Chat client init');
    };
    return self;
}