var _ = require('underscore');
var console = require('mag')('PG');

module.exports = ProceduralGenerator;

function ProceduralGenerator(){
    var self = this;

    // get object random key
    self.key = function (input) {
        return _.shuffle(_.keys(input))[0];
    }

    // parse values from object
    self.parse = function(input){
        var result = {}
        _.each(input, function (value, key) {
            result[key] = self.make(value);
        })
        return result;
    }

    self.make = function (input) {
        if(typeof self[input.type] !== 'function'){
            console.log('Incorrect input type: ' + input.type);
            return;
        }
        return self[input.type](input.values);
    }

    self.number = function (values) {
        if(values.length > 2){
            console.log('Incorrect values array size' + values);
            return;
        }
        var min = values[0];
        var max = values[1];
        return Math.floor(Math.random() * (max - min + 1) + min);
    }

    self.array = function (values) {
        return values[Math.floor(Math.random() * values.length)];
    }

    return self;
};