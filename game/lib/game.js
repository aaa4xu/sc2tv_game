var _ = require('underscore');

module.exports = Game;

function Game(seed) {
    var self = this;
    var player = seed.player;
    var quests = seed.quests;

    //TODO maybe try ES6 Proxy to emulate magic getters & setters ?

    self.get_quest = function (index) {
        return quests[index];
    }

    self.get_quests = function () {
        return quests;
    }

    self.get_player = function () {
        return player;
    }

    // render HTML
    self.output = function () {

    }
    
    self.get_instance = function () {
        var quests_arr = [];
        _.each(quests, function (quest, key) {
            quests_arr.push(quest.get_instance());
        })
        var instance = {
            player: player.get_instance(),
            quests: quests_arr
        };
        return instance;
    }

    return self;
}