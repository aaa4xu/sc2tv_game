module.exports = Quest;

function Quest(seed){
    var self = this;
    var finished = false;
    var progress = 0;
    var intro = seed.intro;
    var steps = seed.steps;
    var end = seed.end;

    self.next_step = function () {
        progress++;
    }
    
    self.finish = function () {
        finished = true;
    }

    self.get_intro = function () {
        return intro;
    }

    self.get_steps = function () {
        return steps;
    }

    self.get_end = function () {
        return end;
    }

    self.get_instance = function () {
        var instance = {
            intro: self.get_intro(),
            steps: self.get_steps(),
            end: self.get_end(),
        };
        return instance;
    }
}