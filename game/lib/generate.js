var PG = require('./pg');
var Game = require('./game');
var Player = require('./player');
var Quest = require('./quest');

var game_cfg = require('../config/game');
var quest_cfg = require('../config/quest');
var parts_cfg = {
    puzzle: require('../config/puzzle')
}

var pg = new PG();

module.exports = function GenerateGame(){
    var self = this;
    var quests = [];
    var player = null;
    var game = null;

    self.quest = function (type) {
        var quest = {}
        var parts_count = pg.make(quest_cfg[type]['length']); // get parts count for quest

        quest['intro'] = pg.make(parts_cfg[type]['intros']);
        quest['steps'] = {};
        for(var i = 0; i < parts_count; i++){
            quest['steps'][i] = {};
            for(var k = 0; k < 3; k++){
                quest['steps'][i][k] = pg.make(parts_cfg[type]['steps']);
            }
        }
        quest['end'] = pg.make(parts_cfg[type]['ends']);

        return quest;
    }

    self.quests = function () {
        var quests_count = pg.make(game_cfg.globals.quest_count);
        for(var i = 0; i < quests_count; i++){
            var type = pg.key(quest_cfg);
            quests[i] = new Quest(self.quest(type));
        }
    }

    self.player = function () {
        var player_seed = pg.parse(game_cfg.player);
        player = new Player(player_seed);
    }

    self.game = function () {
        var game_seed = {
            player: player,
            quests: quests
        };
        game = new Game(game_seed);

    }

    self.init = function () {
        self.player();
        self.quests();
        self.game();
        return game;
    }

    return self;
}