module.exports = {
    goto: {
        type: 'move',
        coords: []
    },
    die: {
        type: 'health'
    },
    win: {
        type: 'game'
    },
    lose: {
        type: 'game'
    }
};