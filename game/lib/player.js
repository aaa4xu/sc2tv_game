module.exports = Player;

function Player(seed) {
    var self = this;
    var health = seed.health;
    var money = seed.money;

    //TODO maybe try ES6 Proxy to emulate magic getters & setters ?

    self.get_money = function () {
        return money;
    }

    self.get_health = function () {
        return health;
    }

    self.get_instance = function () {
        var instance = {
            health: self.get_health(),
            money: self.get_money()
        };
        return instance;
    }

    return self;
}