var commands = require('./lib/commands');
var Generate = require('./lib/generate');
var io = require('socket.io');
var console = require('mag')('GameServer');
module.exports = GameServer;

var rooms = {
    start: {
        poll: {
            0: {
                text: 'Мир',
                weight: 1
            },
            1: {
                text: 'Труд',
                weight: 2
            },
            2: {
                text: 'Май',
                weight: 3
            },
        }
    }
};

function GameServer(){
    var self = this;
    var port = 3002;
    var current_room = null;
    var game = null;

    self.dialog_start = function () {
        var poll = rooms[current_room]['poll'];
        io.emit('poll_start', {poll: poll});
        console.log('Server: New poll started');
    }
    
    self.dialog_end = function (data) {
        console.log('Poll ends. The winner is option ' + data.index);
    }
    
    self.start_io = function () {
        io = io().listen(port);

        io.on('connection', function (client) {
            client.on('join', function () {
                self.dialog_start();
            });

            client.on('poll_end', function (data) {
                self.dialog_end(data);
            })
        });

    }

    self.start = function () {
        var game = new Generate();
        game = game.init();
        current_room = 'start';
        self.start_io();
        console.log('Game server init');
        console.log('Game instance: ' + JSON.stringify(game.get_instance()));
    }
    return self;
}