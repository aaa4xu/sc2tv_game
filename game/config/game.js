module.exports = {
    globals: {
        quest_count: {
            type: 'number',
            values: [5, 10]
        }
    },
    player: {
        health: {
            type: 'number',
            values: [10, 100]
        }, // hitpoints at start
        money: {
            type: 'number',
            values: [0, 100]
        } // money amount at start
    }
}
