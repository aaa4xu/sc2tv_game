module.exports = {
    intros: {
        type: 'array',
        values: [
            'Привет, я хочу сыграть с тобой в игру.',
            'Моя мама всегда говорила: «Жизнь как коробка шоколадных конфет: никогда не знаешь, какая начинка тебе попадётся»',
            'Я собираюсь сделать вам предложение, от которого вы не сможет отказаться'
        ]
    },
    steps: {
        type: 'array',
        values: [
            {
                text: 'Option 1',
                weight: 30, // in percents
                action: 'move',
                direction: 'forward'
            },
            {
                text: 'Option 2',
                weight: 10,
                action: 'die' // kill player & end game
            },{
                text: 'Option 3',
                weight: 50,
                action: 'fail' // fail quest
            },{
                text: 'Option 3',
                weight: 80,
                action: 'finish' // end quest & get reward
            }
        ]
    },
    ends: {
        type: 'array',
        values: [
            {
                text: 'Reward text 1',
                type: 'money',
                amount: [5, 10] // [min, max]
            },
            {
                text: 'Reward text 2',
                type: 'item',
                group_id: [1], // group id
                item_id: 1, // exact item ID (e.g. for artifacts)
                count: [1, 10] // count [n] or in range [min, max]
            }
        ]
    }
}