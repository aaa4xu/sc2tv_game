module.exports = {
    puzzle: {
        length: {
            type: 'number',
            values: [3, 5] // [min, max] minimum length is 3: [intro, puzzle, reward]
        }
    }
}
