var assert = require("assert");
var rewire =  require('rewire');
var sinon = require('sinon');
var EventEmitter = require("events").EventEmitter;
var socketIoMock = {};
var Chat = rewire('../chat/');

var roomMock = { poll: {
    0: {
        weight: 1
    },
    1: {
        weight: 2
    },
    2: {
        weight: 3
    },
    3: {
        weight: 1
    },
}};
var ee = null;

describe('Chat', function() {
    before(function(){
        var servicesMock = {
            listen: sinon.stub()
        };

        Chat.__set__({
            'io': socketIoMock,
            'cfg': {
                testservice: {},
                testservice2: {},
            },
            'console': sinon.stub(require('mag')('Mock'))
        });
    });

    beforeEach(function() {
        ee = new EventEmitter;
        socketIoMock.connect = sinon.stub().returns(ee);
    });

    function initChat(done) {
        ee.on('join', ee.emit.bind(ee, 'poll_start', roomMock));

        ee.on('poll_end', done);

        var chat = new Chat;
        chat.start_io();
        ee.emit('connect');

        return chat;
    }

    it('should detect winner', function(done) {
        var chat = initChat(function(result) {
            assert.equal(result.index, 1);
            done();
        });

        chat.add_vote('testservice', '2', 1);
        chat.add_vote('testservice2', '2', 2);
    });

    it('should ignore duplicate votes', function(done) {
        var chat = initChat(function(result) {
            assert.equal(result.index, 0);
            done();
        });

        chat.add_vote('testservice', '2', 1);
        chat.add_vote('testservice', '2', 1);
        chat.add_vote('testservice', '1', 2);
    });

    it('should ignore votes after poll is finished', function(done) {
        var chat = initChat(function(result) {
            assert.equal(result.index, 3);

            chat.add_vote('testservice', '1', 1);

            done();
        });

        chat.add_vote('testservice', '4', 1);
    });

    it('should corrent start two polls in a row', function(done) {
        var chat = initChat(function(result) {
            if(result.index === 3) {
                ee.emit('poll_start', roomMock)
                chat.add_vote('testservice', '1', 1);
                return;
            }

            if(result.index === 0) {
                done();
            }
        });

        chat.add_vote('testservice', '4', 1);
    });
});
